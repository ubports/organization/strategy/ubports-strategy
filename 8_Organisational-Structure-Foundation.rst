Organisational structure of the Foundation
******************************************


8. Organisational structure of the Foundation

Legally formalised in 2019 the foundation is in the process to professionalisation. This is needed to facilitate its community, the development of Ubuntu Touch and guarantee its maintenance. A more formalised organisational structures are in progress but further work is needed to get the new processes in place.

*One of the lessons learned is that in practice, the unpaid board-members should be disconnected from operational software development, while having still sufficient feeling with the organizational tasks. Of course this is extremely difficult in a community comprising mainly developers.*

Related to board-members tasks
=====

The foundation is there to enable and strengthen the community members with their activities. Additionally, they are thereto establish the contact with the partners (commercial, hardware, vendor).

The foundation should be a facilitator between the (commercial) partners and community members. Partners could provide paid or unpaid work. In exchange the community members provide them with the services needed to make their devices viable and supported over their lifetime. Possible ways to enhance this could be:

-   Q&A sessions between hardware vendors and developers

-   Forum categories co-lead by a hardware vendor

-   Volunteer / paid vacancy category on the website


Most importantly the UBports Foundation helps to organise the community activities and oversees the framework. Additionally they act as a steward for the community's assets. To enable this the foundation needs to focus on finding project owners within the community (members, contributors, collaboration partners).

**Development of the organisation**

The foundation should focus on making it easy for all members including the Board to do their tasks. Self-service, automation and outsourcing could help with this.

**Ease of administration**

-   Further develop the organisational chart, how is responsible for what

-   Re-evaluate the Steering-Committees

    -   Fundraising \>\>\> Funding pooking people for money

    -   HR/Community management, onboarding of contributors and organise community activities and efforts.

**Worriless IT infrastructure**

-   Bundle developer services (build servers, CI pipeline, OS installation, notifications) <https://altispeed.com/>

-   Outsource hosting and infrastructure ( build servers, CI pipeline, OS installation, notifications) on a SLA with reaction time of 8 hour

-   Bundle standard office/organisational tools SKA (nextcloud, matomo, etherpad, jitsi) in a self deployable updatable service like Cloudron.



Development of UBports Fund
=====

**Device Vendors**

To help and motivate vendors to use, develop and support UT. We could help them to employ our community members. UBports receives significant amount of donations above \[5000 euro\]. These funds could be used to match the resources of independent device vendors to make it possible to hire community members. It will create responsibility from the vendors side, they will work on UT. This prevents paying people directly through the community.

**Development of Innovation Lab**

Innovation lab could be a ongoing project where developers could work on coding that would be not easily placed under a regular defined project within the Ubports foundation. Experimentation with new technologies and figuring out development pathe for the future could fit under this.

**Cooperation/ Partnership on external projects**

Working with external organisations on shared projects could strengthen our vision and that could help our organisation to grow. The following cooperations are planned:

-   ()\[official projectname\] R&D project?
Project partners:
- 
