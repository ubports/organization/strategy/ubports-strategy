User Community
*************

The community should be enabled by the UBports Foundation to port as many devices as possible, make apps and maintain the base code source. The community as it should not be a service supplier to (device) partners.

Involvement in the UBports community could help members to enhance their skill set and potentially be employed by commercial device vendors or in mobile computing industry. Spreading the UBports values **to ensure privacy and freedom of choice as the greatest good.**

As a foundation we have the responsibility to supply in the needs of our community members. We have to enable them to function as optimal as possible within the set goals and mandate of the UBports Foundation.

Contributor infrastructure
-------

We have been successful in 2021 to gather new potential contributors, 4 new registration per week, now for 2022 we need to convert (conversion) this to active participation in the community.

To help increase participation we will and are taking the following steps:

1. The board will oversee the persons responsible for the onboarding and development of our community members (HR) for each committee

2. Function descriptions and vacancy pages with volunteering positions

3. Appointing per project a head of steering and provide them with a proper framework how to deal with new contributors

4. Documentation, online and live courses for developer training

    - To do this efficiently we need to make an inventory of all the course materials and maintainers so we can build further on that.

5. Bug-finder, a platform for contributors to have an overview of all open issues for the UBports project. In this way they have a quick overview where help is needed.

6. UBports University an initiative where open-soure mobile Linux developers can be trained an certified. This is to boost Ubuntu Touch development and enhance their personal career path.

Contributors community governing process
--------

-   To get project responsibility embedded in our organisation we should establish a **contributors community governing process**.

    -   Roster of leadership rolls, monthly viewed reviewed by the community on a online page with a transparent process.

    -   Members should be able to regulate and make each other accountable.

-   Community portal should enable easy matching between (development) tasks and meeting place with device vendors.

-   Knowledge building and sharing.

    -   We could organise development sprints or hackathons

    -   Create/ support instructional video's and online training

Better knowledge sharing
---------

Transparency, repo organisation, issue/task organisation, code documentation and maintainership will help (new) members to evolve and be more involved with the UBports community.

> To help contributors to find the right information and to understand Ubuntu Touch and its architecture we need to develop an architecture diagram. This scheme will be a system mapping with links to correct repository that will have the code with a readme consisting information about the maintainer and documentation ( e.g. https://googlechrome.github.io/lighthouse/treemap/ ). This will help with better knowledge sharing and efficient working.
