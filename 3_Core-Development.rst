Development
***********


In the last years a lot has been happening for Linux on mobile devices. This pushed the efforts in development of mobile Operating Systems in the Linux landscape. Since there is a lot of development in this sector we want to strengthen the overall community efforts instead of duplicating efforts of others. Additionally Ubports and her developments needs to stay relevant in the Linux for mobile sphere. It is important to address these strategic focus points the coming year.

**How do keep our open-source stack sustainable:**

-   We contribute to and distribute software open-source how do we ensure (marketing wise) that users and other developer communities contribute back to our source-code


-   How much extra effort will we contribute on a cross project level (e.g. Lomiri in Debian), how do we benefit and how do people (marketing wise) know it is a "investment" of the UBports foundation. How will we benefit from this effort.

-   What is our relation with Ubuntu, since we removed all cannonical/ Ubuntu relations in Lomiri

**How to maintain the whole software stack realistically:**

-   How do we manage our own timeline, while being dependent of other changing software components.

-   Should we make some of our software stack partly rolling? (e.g. broadband library, critically packages ect.

-   What would be the effect if we change base OS since Lomiri is decoupled from ubuntu.

-   Which release model would work best for our target group and development team? Point release, Rolling release, Interim release Standard Support, Long Term Support release

-   Should the Ubports core-team only develop and support the device independent parts. Device specific parts should be carried by the community and the device vendors.


11.0 System software development
------

We need to put ample attention to our app development tools, emulator, SDK, side-load applications.

**PDK:** Platform Development Kit for Ubuntu Touch a development environment for creating the Ubuntu Touch platform. This includes a set of tools to manage and run Ubuntu Touch VMs with compilers and packaging tools for making creating the platform a breeze. That would make it easy to start to lower the bar for newcomers to start building software on our platform. https://snapcraft.io/ubports-pdk - https://github.com/ubports/ubports-pdk

3.0 Core development achieved by end of 2022
------

**3.1 User-Experience**

The feedback of the end-users needs to be analysed, categorized and prioritized. The basic functionality offered by the hardware should be addressed and made available through the user interface, rather than through a tech-savvy person or ICT-system.

**3.2 Functionality and updated software**

Functionality is still to be achieved for Bluetooth (the ability to make hands free calls), fingerprint reader, automatic screen brightness, GPS and camera. In addition, functionality will be required for VoLTE, the high speed communication technology that in some countries has already replaced current calling technologies. This applies to devices with both an Android- and native Linux-base. The next priority is to make sure that the software is up-to-date, consisting of the following components *(the list is built up from the most fundamental components to the least fundamental components)*:

**3.2.1 HAL**

**Hardware Abstraction Layer:** A HAL defines a standard interface for hardware vendors to implement. Using a HAL allows the vendor to implement functionality without affecting or modifying the higher level systems. We will call this abstraction layer a software adapter, or "adapter" for short. We need to keep upgrading Halium versions. The newest devices (highest segments) often contain the newest hardware components. Most of the time, only the camera - and phone manufacturers, or Google knows how they function. Newer Android version makes unlocking newer hardware easier and necessary to make use of these new features. More often than not, it's required to tweak the device to make certain hardware components work. Over the years the UBports porter community has gained a lot of skill in transferring the Android optimizations and features to the UT software.

Halium consists of:

-   **General software part deals with Android OS framework and is "open-source".**

-   **Vendor/ device implementation is hardware specific and "closed-source". These blobs are often already reverse-engineered by other communities and readily available.**

We now have an adapter for Android5, Android7 and Android9+ devices. **To make HALium support sustainable the goal should be:**

i.  HALium 9+ (-- 10 -- 11) should be finalised for release, and development on HALium 12 should start.

ii. Make Halium more generic and usable for more distributions to share code and development efforts. The more generic the software, the easier it is to make it work. Knowing it's a closed environment, a form of reverse engineering needs to be used to find a working solution.

iii. Have a project plan, facilitate maintainers, organise the repo in a easy to find codebase and keep and documentation up to date

iv. Most of the current Android-devices run on Android9. Non Android (9/+) Treble HALium version will be dropped by the core develop team. HALium 5 + 7 need to be marked End Of Live and will be only supported by community effort.

**3.2.2 Middleware:** Project Halium also aims to standardize the middleware software used by various projects to talk with android daemons and make use of the hardware, for instance:

-   OFono/RILd

-   Camera service

-   Pulseaudio / Audiofingerglue

-   GPS

-   Media codecs

-   PIM

**3.2.3 Project VoLTE**:

((SO I SEE THERE IS A PROBLEM WITH VOLTE BUT WHAT IS THE SOLUTION,STRATEGY HERE))

A big challenge in 2022 will be VoLTE. Mobile operators are phasing out their 2G/3G UMTS circuit-switched networks and are upgrading to VoLTE (Voice Over LTE) packet-based networks services. So essentially VoLTE amounts to mobile calls over LTE, better known as 4G, with an IP address instead of a phone number.

*In the US, Verizon customers can no longer activate new devices without VoLTE capabilities. AT&T has already shut down its 2G and 3G networks, T-Mobile will shut down these networks from the 1st July 2022 and Verizon will do the same by the end of 2022. If by that date your device (and operating system) doesn't support VoLTE, you can't make any calls with your **Android native device**. That means **all UT devices** (and other mobile Linux OS devices) except the PinePhone* which is a native Linux device.

Since 2021 the UBports Foundation is sponsoring research to replace the functionality of the proprietary IMS service that we need for VoLTE calls.https://ubports.com/blog/ubports-news-1/post/first-steps-in-volte-support-for-ubuntu-touch-3768

A time consuming part is that software needs to be developed for each separate modem type implemented by the device vendors. There are two proposed solutions; **Proprietary IMS Service in Halium** and **Open Source IMS Implementation.** The most desirable one is the latter, you can find both strategies in https://gitlab.com/ubports/development/project-management/-/blob/master/VoLTE/UT-VoLTE-proposed-technical-architecture.pdf


**3.2.4 Operating System**

**Ubuntu Upgrade:** Based on Debian, Ubuntu is one of the most stable OSes in the world, when you go for daily driver and your users are non technical people, this is the way to go. The newer Ubuntu version 20.04 will give many advantages in hardware and software support. We are now running Ubuntu Touch on Ubuntu 16.04 LTS and this will be upgraded to 20.04 LTS in mid 2022. The big change here is the switch to Systemd:

-   *Methodology by which an operating system is booted. It's a software suite that provides an array of system components for Linux operating systems. Its main aim is to unify service configuration and behaviour across Linux distributions; Systemd\'s primary component is a \"system and service manager\"---an init system used to bootstrap user space and manage user processes. It also provides replacements for various daemons and utilities, including device management, login management, network connection management, and event logging.*

Canonical supports the LTS (long time support) for five years. The updates and security patches are now backported by the team to keep 16.04 secure because standard support ended in April 2021. Now we should come to an agreement with Canonical to receive extended security support for 16.04 LTS until the upgrade to 20.04 is done.

**Ubuntu LTS → ISS (2024):** The LTS version is supported for 5 years, which means that by now we have only 3 years left until we need to upgrade to 24.04 in 2025. From that point it would be a desired option to switch to follow the Interim release Standard Support (ISS) release cycle again. Having a mobile phone distro on long term support means we always will have outdated packages of everything, and are constantly struggling in an ever-changing IT world. Having an ISS or even rolling release could be better workable because we than be upgrading constantly smaller parts of our OS that should be easier than locking down everything every 3 years or so.

**3.2.5 Desktop Environment - Prioritise Lomiri**

**Lomiri:** is the graphical desktop environment of Ubuntu Touch. For the user we could call it the face of UT, for this reason we should prioritize its development within UBports.

-   Why Lomiri? Because of its convergent desktop feature: Just one code base for all form factors, usable on a phone, tablet and desktop.

-   Why Lomiri for mobile devices? Due to its huge Canonical codebase legacy it has most mobile features and indicators already built in. To compete with stock Android this is the most complete choice.

-   Why Lomiri for Debian derivatives? Uploading software to Debian is always the best approach for bringing software into other distributions that are constantly derived from Debian (e.g. just like Ubuntu).


Investing in adoption of Lomiri as a graphical desktop environment by more Linux distributions is very important to ensure its overall future. At the same time this will accelerate the development of Lomiri and also distributes the maintainer load across multiple development resources. Lomiri is best suited for a future centred around convergence, especially when you compare it to its alternatives.

Convergence is a unique selling point for Lomiri on Ubuntu Touch what should make it a priority. We should get clarity on the following points:

-   How can we accelerate the adoption of convergence on UT devices?

-   What are the problems and how can we tackle this?

-   Should we make screen-cast dongle with Pine64, so that we are not dependent of the usb/hdmi ports on the devices?

Since 2020 the UBports team have been working to remove canonical trademarks in order to get Lomiri "licence free" and adopted by other desktop and mobile distros. More importantly there has been a huge effort to get Lomiri accepted upstream in Debian so it will be automatically available in core Ubuntu. This process is nearly finished and should be in Debian and derived distros by September 2022. In September Ubuntu LTS will sync with Debian upstream.

-   In 2022 the packaging of Lomiri for upstream Debian will be finalised. We should focus on finding maintainers/ partners (desktop and mobile distros) like Debian, Ubuntu UnityX and Manjaro to push Lomiri (+ Mir + Qt) into mainstream and potentially pre-installed on the desktop and mobile.

**3.2.6 Qt is a framework suited for mobile use as the CPU load for rendering is much smaller than with for example GNOME.**

**Update Qt:** free and open-source widget toolkit for creating graphical user interfaces as well as cross-platform applications that run on numerous software and hardware platforms such as Linux, Windows, macOS, Android and other embedded systems with little to no change in the underlying codebase while still remaining a platform-native application with native capabilities and speed.

-   A newer Qt version also has numerous advantages and is also a must have. The version of Qt should be upgraded to 5.12 (5.13 is released OpenSource) now. It's disconcerting that the software will switch to a more closed licence after that [1]_ .

[1]_ *KDE Free Qt Foundation -- https://kde.org/community/whatiskde/kdefreeqtfoundation/ → there's an opportunity to support and contribute to this initiative.*

**Ayatana Indicators:** These indicators are all individual services, e.g. the wifi indicator provides not only the wifi information but more importantly the wifi service itself.

