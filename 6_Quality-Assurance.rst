Quality Assurance
****************

[Needs an explanation on what Quality Assurance is]

**Open-Cuts.org:** An open crowdsourced user-testing suite by UBports Foundation. Where testers can easily report the bugs with their ported device. UBports has about 80 devices that the community has ported, Open-Cuts checks in its database the (feature) supportability of the devices. In this crowdsourced method we get:

-   better supported devices

-   higher engagement by community members on testing

Aggregation caching needs to be implemented to deal better with the data queries and speed up the device checks.

**Additionally it would be great to create a team around the maintenance of this project and integrate crowdsourced testing further in the UBports community.**



**Allthebugs >> Bug-finder:**

We have a lot of contributors that would like to help with tasks in the UBports project. It would be helpful to create a single website where all issues, feature requests and general needs of the Ubports set of projects can be browsed, searched, filtered and assigned. In this way the tasks load could be shared better. An aggregator site that uses Github/Gitlab APIs to fetch issues from all the UBports related projects (several dozen, if not more) and:

-   Lists all issues/needs in a single table

-   Provides a natural search field for keywords

-   Provides filters for certain heuristics like translations

-   Allows for upvote/downvote for popularity of the priority

-   Clearly outlines an owner (issue filer, for example) to get in touch with

-   LATER: Bounty system, let people \"Patron\" a specific issue/need and add a \$ figure

-   LATER: Make the Patron system a bit of an action based solution

-   LATER: Let users sign up for specific types of needs that get filed, notifications
