APP development
***************

Apps are one of the key elements of user experience on a mobile device. Unfortunately user standards are been benchmarked by the closed-source dominating ecosystems and services of Google and Apple. The bar is high and one has to understand that developing applications is more an ideological motive than a financial incentive. Therefore the Foundation has a responsibility to make the development of apps as accessible as possible for developers.

**Collective app development:** With a collective community effort we could achieve a higher quality and maintenance of apps. Additionally we could better connect to the user demand of essential apps in order to create a sustainable success for our user-base to use UT as daily driver.

**Clickable:** A container that allows applications of the ARM architecture to be run on your computer. It will have all the necessary tools and source code to compile an application for Ubuntu Touch.

**Skills:** We need to have a good overview who leads the development of which app. Additionally app development courses and materials should be easily available for our (new) community members.


4.0 Applications
-------

**Core apps - these need to be working 100%.** - the tools to connect to the outside world for the users these and they're already installed on the phone by default, Dialer, Messaging, Address Book (Contacts) App, Calendar, Email client, clock, calculator and **Morph Webbrowser**. The latter is for a mobile device extremely important for online usage.

-   **Web Engine:** The Qt WebEngine that serves as the basis for the (Morph) browser should always be up-to-date. If this is not the case, other websites may not be accessible, may not work properly, may display an outdated browser message, or browser-based applications may not launch. (Example: We have already seen what it means to users when the browser is out-of-date with the Oxide browser as a basis).

    -   **Digital Rights Management (DRM):** Feedback shows the importance of the ability to view DRM content e.g. to watch Netflix, Hulu, Disney+. It is crucial that implementing the WideVine Content Decryption Module (CDM) plug-in is a priority.

        -   **WideVine Content Decryption Module (CDM) plugin:** Widevine is a proprietary digital rights management (DRM) technology from Google. DRM content decryption using Widevine CDM is not currently supported in Linux on ARM64 based platforms due to a lack of a native ARM64 Widevine CDM binary. A wrapper could be made to fake a 32-bit architecture, but that would be a lot of work.

**User feedback shows there is a need to add these apps to the "core apps" category:**

-   Instant Messaging app like FluffyChat/Matrix that is open-source and uses standards protocols.

-   Sync solution CardDAV CalDAV WebDAV like DavX5 (Android) should be developed to make it possible for users to sync their data.

-   Backup solution for example to backup your data to your cloud storage. https://calyxinstitute.org/projects/seedvault-encrypted-backup-for-android

**As Ubports community, we are responsible for the availability and stability of these apps. An important problem to solve is the question, how to share/handover the responsibility for these apps from the creators to the maintainers?**

**Essential apps** - Most wanted apps based on user feedback from device partners. These can cover as alternatives for Android apps functionalities, these are developed and maintained by community members and can be found on Open-store.io. The apps can be a web-app or a native-app and should have a \"popular and widely-used or with strong communities\" license approved by the OSI https://opensource.org/licenses/category.

5.0 Client base
-------

The strategy to make essential apps sustainable is that the UBports foundation plants the seed and to help partners start or speed up development of essential apps. The need of the client base, will motivate the partner to adopt and develop it further. To facilitate this process we need to facilitate a pool of community members interested in app development. We help them develop their skills and easily connect them to (commercial) device vendors to work with.

It's good to understand what's possible and what users want in order make UT consumer ready and grow the user base:

Category I -- Closed-source free apps, with a incoming and outgoing data transaction. The business model of these apps is mostly based on selling the data from the users. The creators of these apps are only interested in large numbers of users. E.g. Instagram, Facebook, Whatsapp are completely closed (The client side source code is closed, the API is closed and the server side is closed). There's no way to see how it works. The only thing you can do is call those companies and ask them if they want to make an alternative for our OS as well, but of course they are never going to cooperate, because it is not economically viable. The only thing they want is large masses of users. Only when our user-base is large enough, they might be willing to cooperate.

Category II - Closed-source apps, with a incoming data transaction only. For which it would make business sense to make an app. These apps are not free either, mostly subscription based, and therefore, the user base doesn\'t have to be so massive (compared to the first category) to make it commercially interesting. An example of this would be Spotify Premium. They often have an open (web) API that can be used.

Category III: Open-source replacement apps, based on existing open-source mobile apps, with a incoming and/or outgoing data transaction. These apps could be used as alternative for existing "mainstream" closed-source apps. In order to be on par with features they are build as native app. The original (android) apps have open protocols - they do not object when an app is created on the device that uses their protocol. Signal or Telegram are examples: their source-code and API protocol is open and usable as long as it works. (source: Telegram surpasses 500 million users as a result of the recent WhatsApp exodus.) This is important, because since we can\'t run WhatsApp, we created Teleports (our alternative to Telegram) that functions properly.

Category IV: Open-source new apps, made by community members, who want to experiment or see the need to cover a process with an application.

Category V: Banking apps: This is a separate category all together. We believe this has to be solved by lobbying efforts within the government, as these days a banking app is a semi-public service. Now a day a lot of personal banking activities (most) are done on mobile devices. This is perhaps the most difficult category to realise, but it's a necessity for long-term success. There are various banks with open banking APIs https://www.bunq.com/api that could be piloted.

Category VI: Waydroid a sandboxed/ container environment for Android apps could be a temporarily solution for the problems described above. Though the installation should be foolproof so the UX should be usable for all users. Potentially this should become a pre-installed core-app.
