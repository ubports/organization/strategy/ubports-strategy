Product
*******

**Product**

For the end-user the product is **a mobile device + software**.

**Core value**

Privacy and freedom of choice for the general public.

**Mission**

Ubuntu Touch to be the **truely free Linux mobile device**.   

**Goal**

Supply as many device collaboration partners as possible with pre-installed Ubunu Touch to reach the general (non-technical) public.

**Objectives**

1. In order to have devices in the shops with pre-installed Ubuntu Touch the following objectives must be met:

- Have good relations with device vendors
- Have an organised code base, so community members can learn and help
- Have a healthy (non device specific) software stack and HALium adapters
- Have the basic functionalities offered by the hardware work  

*Note: Companies will have an economic incentive to push their products with Ubuntu Touch pre-installed on devices in their stores.*

    Most open-source projects only reach mass adoption when commercial services play an important role in the business model of the     companies behind the projects. Without a viable business model the chances of becoming a large project are slim to none. For example:   The Linux kernel project is hosted by The Linux Foundation and focuses on the development of the Linux kernel. It shows that the number of programmers working on it is huge, but the number of volunteers is in comparison impressive but relatively small. The driving force behind the success are developers, who are paid directly or indirectly by companies that have a great interest in the kernel; to keep it secure, guaranteed of maintenance and continuously in development. It is better when everyone with the same interest collaborates on the best product offered. Otherwise, you're taking a risk when continuing the maintenance of your product on your own and in the end being the only one left.

2. Companies where OS plays a role in their business model will contribute in two ways to Ubuntu Touch:

-   Indirect: by making donations to the Foundation. This aids the collaborating partners as their products are constantly being developed.

-   Direct: by hiring people. These collaborating partners hire people themselves to:

    -   Improve specific features for either the device or target audience.

    -   Maintain the device codebase

    -   Provide support for the device

3. Target effective marketing campaigns and design development roadmap to include all potential target groups and markets, including:

-   Linux enthusiasts/ developers

-   Critical thinkers/ pioneers

-   Enterprise clients security and Mobile Device Management

-   End users




