.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the UBports [DRAFT] strategy for 2022 and beyond!
====================================================

Introduction
============

Our goal for UBports is to create a copylefted libre mobile operating system. One you can use, study, change and share; with all.

About UBports
-------------

The project was founded by Marius Gripsgard in 2015 and in its infancy a place where developers could share ideas and educate each other in hopes of bringing the Ubuntu Touch platform to more mobile devices.

After Canonical suddenly announced `plans to terminate support for Ubuntu Touch <https://insights.ubuntu.com/2017/04/05/growing-ubuntu-for-cloud-and-iot-rather-than-phone-and-convergence/>`_ in April of 2017, UBports and its sister projects began work on the source code; maintaining and expanding its possibilities for the future. Today, UBports is a volunteer group, formalised as a charitable foundation. 

Open for suggestions
-------------
You can edit the text with your suggestion and create a merge request. Please enter a reason for the the change request and select Commit changes. A reviewer will check your request and will accept them or come back to you with a response.
Plese find the repository here: https://gitlab.com/ubports/organization/strategy/ubports-strategy/

Table Of Contents
------------
.. toctree::
   :maxdepth: 1
   :caption: Strategy
   :name: Strategy

  1_Product
  2_Hardware
  3_Core-Development
  4_Application-Development
  5_Porting
  6_Quality-Assurance
  7_User-Community
  8_Organisational-Structure-Foundation

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

https://ubports.gitlab.io/organization/strategy/ubports-strategy
