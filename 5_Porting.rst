Porting
********

UBports as its name says is a porting community that from its origin is eager to get Ubuntu Touch running on any device they get their hands on. That is great, the more devices are able to run UT the more people that will use it.

Older devices are easily available and the technology is known. In order to get more newer devices pre-installed with UT the Foundation could help with providing the community with newer devices to port, in cooperation with vendor partners. Further in the process these developers could be hired by these same vendors.

Since the core developers can't help to support all current ported devices a selection should be made which devices should be able to run on the maintained software stack. This selection could be made looking at the following criteria:

-   Devices that are most used

-   Devices that are most wanted

-   Devices that could help UT be pre-installed in the shops

-   Ported chipset

-   VoLTE modem enabled

-   Ubuntu 20.04 enabled

-   Halium 9+ enabled

**Android Linux:** The community has most experience with these devices e.g. Volla. A lot of middleware and driver issues, for example power management, are solved by the Android layer that HALium adapt on.

-   How up to date with Android should the core team stay? It is an intensive task to keep up with the Android versions to deliver stable device images.

**Native Linux:** The community has less experience with these devices e.g. Pinephone. All the vendor blobs need to be re-engineered to let the middleware and driver work in UT. Elements that are not desktop native, like power management while receiving calls. This needs to be developed from scratch.

Some vendors like Shiftphone have a strategy where they start with Android and slowly "Shift" to mainline Linux. They do the Android development in house and gain a lot of knowledge this way. Cooperating with them could be beneficial to create a better connection between android native hardware and UT without to much reverse engineering.

**UBports installer**: A desktop installer to make installing Ubuntu Touch on your device easy without command-line! Next to UT you can install third-party operating systems as well. (very important Volla etc). Lowering the threshold to install UT achieves:

-   higher UT installation success rate

-   more people are able to try UT

-   easier to test UT

-   more different devices that run UT

-   more community members involved in creating device specific configuration files

The development of the installer is officially in a conceptual state and nearing beta release. When this state is reached it will be easier to engage community members in the further development and maintenance of the installer. The codebase is JavaScript so it has an accessible entry level for new developers in the community.

With an online hackaton we could create a team from different OS communities to accelerate the development of the installer and write more plugins to support their OSes to be motivated to maintain.

From the 17.000 devices being ported with the Ubports installer there are about 100 that keep working properly. Obviously his amount needs to increase. [needs checking if properly interpreted]
