UT pre-installed on more new phones
***********************************


Whether we succeed in this depends on the following factors:

**2.1  Excellent support for collaborating partners** (With or without HALium ) For now, this includes:

a.  Pine64 Pinephone Pro upcoming edition (native Linux device)

b.  Volla 22 (business device)

c.  Fairphone 4 (ethical device)

d.  Shiftphone 2022 (convergence device)

e.  Pixel 3a (developer device)

f.  Ftec pro 1 X(launching early Q1 2022), Planet Computers Astro slide (launching Q1 2022), when the request comes from these suppliers and the community can port these phones

*Note: At the launch of the Volla phone, the collaboration did not go as planned and agreements where not met by the foundation. Lessons learned include*

- *the need to manage partner expectations through good collaboration contracts (templated).*

- *the need to facilitate a good connection between UBports community developers and collaboration partners. So they could easily employ community members and setup a support network (of volunteers)*

- *the need to manage expectation towards outside world*

**2. 2 Hardware - Project HALium:** Devices released with an alternative OS (non-google android or native Linux) by hardware vendors (e.g. Pine Phone) that do not choose for Ubuntu Touch, should still be able to run UT on it. To make porting these devices easier we should strive to have stable **HALium adapters** for the latest (3) versions of Android.

**2.3 Hardware - Project UBports installer:** Users that bought a pre-installed Android device should be given an option to switch to Ubuntu Touch. To ensure mass adoption we must strive that UT can be easily installed as an alternative through the UBports installer.

